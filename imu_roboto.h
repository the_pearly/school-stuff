#ifndef imu_roboto_h
#define imu_roboto_h

#include <Arduino.h>
#include <FreeSixIMU.h>
#include <FIMU_ADXL345.h>
#include <FIMU_ITG3200.h>
#include <DFRobot_QMC5883.h>

/**
 * Brief:Define values
 * Notes:
 */


#define VERBOSE 0 // Tell what is doing via Serial. For debugging.

#define _DOWN -1
#define _UP 1
#define _ZERO 0

#define RIGHT_SERVOMOTOR 10 // Pin Right Continuous Servo Motor
#define LEFT_SERVOMOTOR 11 // Pin Left Continuous Servo Motor
#define SONAR_SERVOMOTOR 9  // Pin Sonar Servo Motor

#define RIGHT_INDICATOR_LIGHT 7
#define LEFT_INDICATOR_LIGHT 8

#define	HC_SR04_TRIGGER 3 // Pin for Ultrasonic Rangefinder Trigger.
#define HC_SRO4_ECHO 4 // Pin for Ultrasonic Rangefinder Return Ecoho.
#define STATUS_LED 13  // Pin for Status LED.
#define DEBUG_CYCLE_TRIGGER 12 // Pin for measuring cycle time. For debugging.

#define PROGRAM_SELECT_SWITCH_A 5 // Pin for Program Selector Pushbutton A.
#define PROGRAM_SELECT_SWITCH_B 6 // Pin for Program Selector Pushbutton A.

#define PROGRAM_BIT_A 0 // Value of Program Selector Pushbutton A Bitwise Shift
#define PROGRAM_BIT_B 1 // Value of Program Selector Pushbutton B Bitwise Shift

#define ROW_ZERO 0 // Used in Sonar functions to fill array column A
#define ROW_ONE 1 // Used in Sonar functions to fill array column B

#define _NULL 0x00 // ZERO!
#define TOTAL_PROGRAM_SLOTS 4 // NUMBER OF AVAILABLE PROGRAM SLOTS
#define TOTAL_PROGRAM_LINES 16 // NUMBER OF AVAILABLE PROGRAM LINES

#define PROGRAM_A 0x00
#define PROGRAM_B 0x01
#define PROGRAM_C 0x02
#define PROGRAM_D 0x03

// Action Codes
#define ACTION_CODE_ALLSTOP 0x01
#define ACTION_CODE_FORWARD 0x02
#define ACTION_CODE_TURNRIGHT_ANGLE 0x03
#define ACTION_CODE_TURNLEFT_ANGLE 0x04
#define ACTION_CODE_TURNRIGHT_DUR 0x05
#define ACTION_CODE_TURNLEFT_DUR 0x06
#define ACTION_CODE_REVERSE 0x07
#define ACTION_CODE_REST 0x08

#define ACTION_CODE_FORWARD_DETECT 0x09
#define ACTION_CODE_TURNRIGHT_DETECT 0x0A
#define ACTION_CODE_TURNLEFT_DETECT 0x0B

#define ACTION_CODE_SLOWTURN_LEFT 0x0C
#define ACTION_CODE_SLOWTURN_RIGHT 0x0D

#define ACTION_CODE_GOTO_LINE 0x0E
#define ACTION_CODE_PROGRAM_END 0x0F

#define ACTION_CODE_SENSE 0x10

// Script Durations

#define DURATION_1M 76
#define CIRCLE_DURATION 190

#define WAIT_DURATION 250

#define ANGLE_90 88
#define ANGLE_SLOW 120
#define ANGLE_120 118

// Servo Motor Preset Values
#define MOTOR_RIGHT_FORWARD_VALUE 1420 // Value for Right Servo Motor when Driving Straight
#define MOTOR_LEFT_FORWARD_VALUE 1577 // Value for Left Servo Motor when Driving Straight
#define MOTOR_RIGHT_STOPVALUE 1500 // Value for Right Servo Motor when Stopped
#define MOTOR_LEFT_STOPVALUE 1500 // Value for Left Servo Motor when Stopped
#define MOTOR_RIGHT_RHT_TURNVALUE 1535 // Value for Right Servo Motor when in Right Hand Turn
#define MOTOR_LEFT_RHT_TURNVALUE 1535 // Value for Left Servo Motor when in Right Hand Turn
#define MOTOR_RIGHT_LHT_TURNVALUE 1465 // Value for Right Servo Motor when in Left Hand Turn
#define MOTOR_LEFT_LHT_TURNVALUE 1465 // Value for Left Servo Motor when in Left Hand Turn

//// HeRE
#define MOTOR_RIGHT_RHT_SLOWTURNVALUE 1453 // Value for Right Servo Motor when in Left Hand Turn
#define MOTOR_LEFT_RHT_SLOWTURNVALUE 1577 // Value for Left Servo Motor when in Left Hand Turn

// ADJUST THESE vv FOR SLOW TURN MOTOR VALUES

#define MOTOR_RIGHT_LHT_SLOWTURNVALUE 1410 // Value for Right Servo Motor when in Left Hand Turn
#define MOTOR_LEFT_LHT_SLOWTURNVALUE 1559 // Value for Left Servo Motor when in Left Hand Turn

#define MOTOR_RIGHT_REV_VALUE 1580 // Value for Right Servo Motor when Reversing
#define MOTOR_LEFT_REV_VALUE 1425 // Value for Left Servo Motor when Reversing
// Timing Preset Values
#define CYCLETIME 5 // Maximum number of Milliseconds each main program loop should take.

// Compass Constants
#define DEC_ANGLE_DEG 11.0 // Compass heading Declanation Angle for Melbourne
#define DEC_ANGLE_MIN 38.0 // Compass heading Declanation Angle for Melbourne

// Status Codes
#define STATUS_IS_MOVING 0x01 // Robot is in motion
#define STATUS_ALIGNING 0x02 // Robot is Stopped and attempting to align IMU
#define STATUS_SENSING 0x04 // Robot is Performing a Sensing operation
#define STATUS_COMPUTING 0x08 // Robot is Performing a computation for navigation.

#define STATUS_ALIGN_OK 0x10 // IMU is aligned
#define STATUS_IMU_UPDATE_REQUIRED 0x20 // IMU is not aligned or Requires alignment
#define STATUS_ROBOT_REST 0x40 // Robot is in a stationary rest state.
#define STATUS_ROBOT_NAVIGATING 0x80 // Robot is attempting navigation.

#define RUN_PROGRAM_ZERO // ... 
#define FLAG_SET_TURN_ROLL_OVER 0x01 // If heading is over 2 pi then roll over to zero.
#define FLAG_SET_OBSTACLE_DETECTED_FRONT 0x02 // Obstacle immediately in front requires action.
#define FLAG_SET_OBSTACLE_DETECTED_RIGHT 0x04
#define FLAG_SET_OBSTACLE_DETECTED_LEFT 0x08
#define FLAG_SET_WAY_IS_CLEAR_AHEAD 0x10
#define FLAG_SET_BEST_DIRECTION_LEFT 0x20
#define FLAG_SET_BEST_DIRECTION_RIGHT 0x40

// Sonar
#define NUMBER_OF_SCAN_ANGLES 11
#define NUMBER_OF_SCAN_READINGS 2
#define NUMBER_OF_FRONT_SCAN_ANGLES 3

#define SCAN_ANGLE_15_PWM 2000
#define SCAN_ANGLE_30_PWM 2150 // Sonar servo motor angle 30 degre microseconds
#define SCAN_ANGLE_45_PWM 1975
#define SCAN_ANGLE_60_PWM 1800 // Sonar servo motor angle 60 degre microseconds
#define SCAN_ANGLE_75_PWM 1615
#define SCAN_ANGLE_90_PWM 1430 // Sonar servo motor angle 90 degre microseconds
#define SCAN_ANGLE_105_PWM 1230
#define SCAN_ANGLE_120_PWM 1030 // Sonar servo motor angle 120 degre microseconds
#define SCAN_ANGLE_135_PWM 880
#define SCAN_ANGLE_150_PWM 730 // Sonar servo motor angle 150 degre microseconds
#define SCAN_ANGLE_165_PWM 580

#define SCAN_ANGLE_15_VALUE 75
#define SCAN_ANGLE_30_VALUE 60 
#define SCAN_ANGLE_45_VALUE 45
#define SCAN_ANGLE_60_VALUE 30
#define SCAN_ANGLE_75_VALUE 15
#define SCAN_ANGLE_90_VALUE 0 
#define SCAN_ANGLE_105_VALUE 15
#define SCAN_ANGLE_120_VALUE 30
#define SCAN_ANGLE_135_VALUE 45
#define SCAN_ANGLE_150_VALUE 60
#define SCAN_ANGLE_165_VALUE 75

#define SCAN_ARRAY_POSITION_15 0
#define SCAN_ARRAY_POSITION_30 1
#define SCAN_ARRAY_POSITION_45 2
#define SCAN_ARRAY_POSITION_60 3
#define SCAN_ARRAY_POSITION_75 4
#define SCAN_ARRAY_POSITION_90 5
#define SCAN_ARRAY_POSITION_105 6
#define SCAN_ARRAY_POSITION_120 7
#define SCAN_ARRAY_POSITION_135 8
#define SCAN_ARRAY_POSITION_150 9
#define SCAN_ARRAY_POSITION_165 10

#define MINIMUM_ALLOWABLE_DISTANCE_FRONT 10// Sonar minimum value read directly in front of robot to avoid collision
#define MINIMUM_ALLOWABLE_DISTANCE_LEFT 15// Sonar minimum value read directly in front of robot to avoid collision
#define MINIMUM_ALLOWABLE_DISTANCE_RIGHT 15// Sonar minimum value read directly in front of robot to avoid collision

#define SONAR_SWEEP_DELAY 50 // Sonar servo motor movement time. 
#define SONAR_POSITION_HOLD_DELAY 200
/**
 * Brief: Data Struct
 * Notes: Robot Data structure. 
 */
 
typedef struct __robotData_Typedef
{
  
  int32_t heading = 0; 
  
  int32_t previousyawrotation = 0;
  int32_t yawrotation = 0;
  int32_t yawCompensation = 0;
  int32_t targetrotation = 0;
  
  uint8_t programLineCount = 0; 
  uint8_t programSwitchValue = 0;  
  uint32_t rangeFinderValues = 0;
  
  float declinationAngle;
  
  uint8_t navigationCommand[3] = {ACTION_CODE_ALLSTOP, _NULL, _NULL};
  
  uint16_t sonar_Position_Angle[NUMBER_OF_SCAN_ANGLES] = {SCAN_ANGLE_15_PWM,
														SCAN_ANGLE_30_PWM,
														SCAN_ANGLE_45_PWM,
														SCAN_ANGLE_60_PWM,
														SCAN_ANGLE_75_PWM,
														SCAN_ANGLE_90_PWM,
														SCAN_ANGLE_105_PWM,
														SCAN_ANGLE_120_PWM,
														SCAN_ANGLE_135_PWM,
														SCAN_ANGLE_150_PWM,
														SCAN_ANGLE_165_PWM};
  
  uint16_t sonar_Distance_Measurement[NUMBER_OF_SCAN_READINGS][NUMBER_OF_SCAN_ANGLES] = {{0}};
  uint8_t sonar_Position_Value[NUMBER_OF_SCAN_ANGLES] = {SCAN_ANGLE_15_VALUE,
														SCAN_ANGLE_30_VALUE,
														SCAN_ANGLE_45_VALUE,
														SCAN_ANGLE_60_VALUE,
														SCAN_ANGLE_75_VALUE,
														SCAN_ANGLE_90_VALUE,
														SCAN_ANGLE_105_VALUE,
														SCAN_ANGLE_120_VALUE,
														SCAN_ANGLE_135_VALUE,
														SCAN_ANGLE_150_VALUE,
														SCAN_ANGLE_165_VALUE};
  uint8_t bestPathLeft = SCAN_ANGLE_45_VALUE;	
  uint8_t bestPathRight = SCAN_ANGLE_45_VALUE;

  uint8_t frontScanPositionCounter = SCAN_ARRAY_POSITION_75;
  int8_t frontScanDirection = _UP;
  
}RobotData_Typedef;

/**
 * Brief: Status Struct
 * Notes: Robot Status structure. 
 */
 
typedef struct __robotStatus_Typedef
{

  uint8_t robotStatus = _NULL;
  uint8_t robotFlagSet = _NULL;  
	
}RobotStatus_Typedef;

/**
 * Brief: Timers Struct
 * Notes: Robot Data structure. 
 */
 
typedef struct __robotTimers_Typedef
{
  
  unsigned long cycleTimer = 0;
  unsigned long movementTimer = 0;
  unsigned long scanTimer = 0;
  
}RobotTimers_Typedef;

#endif