#ifndef imu_roboto_h
#define imu_roboto_h

#include <Arduino.h>
#include <FreeSixIMU.h>
#include <FIMU_ADXL345.h>
#include <FIMU_ITG3200.h>
#include <DFRobot_QMC5883.h>

/**
 * Brief:Define values
 * Notes:
 */

//

#define _NULL 0x00
#define TOTAL_PROGRAM_LINES 16

// Action Codes
#define ACTION_CODE_ALLSTOP 0x01
#define ACTION_CODE_FORWARD 0x02
#define ACTION_CODE_TURNRIGHT_ANGLE 0x04
#define ACTION_CODE_TURNLEFT_ANGLE 0x08
#define ACTION_CODE_TURNRIGHT_DUR 0x10
#define ACTION_CODE_TURNLEFT_DUR 0x20
#define ACTION_CODE_REVERSE 0x40
#define ACTION_CODE_SENSE 0x80

// Servo Motor Preset Values
#define MOTOR_RIGHT_FORWARD_VALUE 1505
#define MOTOR_LEFT_FORWARD_VALUE 1600
#define MOTOR_RIGHT_STOPVALUE 1500
#define MOTOR_LEFT_STOPVALUE 1500
#define MOTOR_RIGHT_RHT_TURNVALUE 1600
#define MOTOR_LEFT_RHT_TURNVALUE 1600
#define MOTOR_RIGHT_LHT_TURNVALUE 1400
#define MOTOR_LEFT_LHT_TURNVALUE 1400

#define MOTOR_RIGHT_REV_VALUE 1600
#define MOTOR_LEFT_REV_VALUE 1505
// Timing Preset Values
#define CYCLETIME 5

// Compass Constants
#define DEC_ANGLE_DEG 11.0
#define DEC_ANGLE_MIN 38.0

// Status Codes
#define STATUS_IS_MOVING 0x01
#define STATUS_ALIGNING 0x02
#define STATUS_SENSING 0x04
#define STATUS_COMPUTING 0x08

#define STATUS_ALIGN_OK 0x10
#define STATUS_IMU_UPDATE_REQUIRED 0x20

#define FLAG_SET_TURN_ROLL_OVER 0x01
/**
 * Brief: Typedef Struct
 * Notes: 
 */
 
typedef struct __robotData_Typedef
{
  
  int32_t heading = 0;
  int32_t previousyawrotation = 0;
  int32_t yawrotation = 0;
  int32_t yawCompensation = 0;
  int32_t targetrotation = 0;

  float declinationAngle;

}RobotData_Typedef;

typedef struct __robotStatus_Typedef
{

  uint8_t robotStatus = _NULL;
  uint8_t robotFlagSet = _NULL;  

}RobotStatus_Typedef;

typedef struct __robotTimers_Typedef
{
  
  unsigned long cycleTimer = 0;
  unsigned long movementTimer = 0;  
  
}RobotTimers_Typedef;

#endif