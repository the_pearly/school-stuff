﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

/**
 * Project Description :
 * Authors :
 * 
 */

/**
 * Brief:Sensor Includes
 * Notes:
 */

#include "imu_roboto.h"

/**
 * Brief:Other Includes
 * Notes:
 */

#include <Wire.h>
#include <Servo.h>
//Beginning of Auto generated function prototypes by Atmel Studio
void allStop(RobotStatus_Typedef* Status);
//End of Auto generated function prototypes by Atmel Studio



/**
 * Brief:Peripheral Management Objects
 * Notes:
 */

FreeSixIMU sixDOF = FreeSixIMU();
DFRobot_QMC5883 compass;
Servo servoRight;
Servo servoLeft;

/**
 * Brief:Setup
 * Notes:
 */

/**
 * Brief:Prototype Functions
 * Notes:
 */
 
void update_heading(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void forward(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void forward(RobotStatus_Typedef *Status);
void turnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000);
void turnRight(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void turnLeft(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000);
void turnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void reverse(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void updateSensors();

void setup() 
{
  return;
}


/**
 * Brief: PROGRAM ENTRY
 * Notes: Enter up to 16 program actions into this array.
 *        See imu_robot.h for action codes
 */


uint8_t Program[TOTAL_PROGRAM_LINES][2] = 
{
  // Action Code , Duration/Angle/Sensor
  { ACTION_CODE_FORWARD , 2 },
  { ACTION_CODE_TURNRIGHT_ANGLE , 90 },
  { ACTION_CODE_TURNLEFT_ANGLE , 90 },
  { ACTION_CODE_TURNRIGHT_DUR , 1 },
  { ACTION_CODE_TURNLEFT_DUR , 1 },
  { ACTION_CODE_SENSE , _NULL },
  { ACTION_CODE_REVERSE , 1 },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL },
  { ACTION_CODE_ALLSTOP , _NULL }
};


/**
 * Brief:Main Loop
 * Notes:
 */

void loop() 
{
  
  RobotData_Typedef roboData;
  RobotStatus_Typedef roboStatus;
  RobotTimers_Typedef roboTimers;

  roboData.declinationAngle = (DEC_ANGLE_DEG + (DEC_ANGLE_MIN / 60.0)) / (180 / PI);

  Serial.begin(115200);
  Wire.begin();
  compass.begin();
  
  delay(5);
  sixDOF.init(); //begin the IMU
  delay(5);

  pinMode(13,OUTPUT);
  pinMode(12,OUTPUT);
  
  while(1)
  {
    
    for (uint8_t programLineCount = 0; programLineCount < TOTAL_PROGRAM_LINES;)
    {
      switch ( Program[programLineCount][0] )
      {
        case _NULL:
          break;
        case ACTION_CODE_ALLSTOP:
          allStop(&roboStatus);
          programLineCount++;
          break;
          
        case ACTION_CODE_FORWARD:
          forward(&roboStatus, &roboTimers, ((uint16_t)(Program[programLineCount][1]*1000)));
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;
          
        case ACTION_CODE_TURNRIGHT_ANGLE:
          turnRight(&roboData, &roboStatus, ((uint16_t)(Program[programLineCount][1]*100)) );
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;

        case ACTION_CODE_TURNLEFT_ANGLE:
          turnLeft(&roboData, &roboStatus, ((uint16_t)(Program[programLineCount][1]*100)) );
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;

        case ACTION_CODE_TURNRIGHT_DUR:
          turnRight(&roboStatus, &roboTimers, ((uint16_t)(Program[programLineCount][1]*1000)));
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;
          
        case ACTION_CODE_TURNLEFT_DUR:
          turnLeft(&roboStatus, &roboTimers, ((uint16_t)(Program[programLineCount][1]*1000)));
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;
          
        case ACTION_CODE_REVERSE:
          reverse(&roboStatus, &roboTimers, ((uint16_t)(Program[programLineCount][1]*1000)));
          if ( !((roboStatus.robotStatus) & (STATUS_IS_MOVING)) )programLineCount++;
          break;
                  
        case ACTION_CODE_SENSE:
          updateSensors();
          programLineCount++;
          break;             
      }
      
      digitalWrite(12,HIGH);
      update_heading(&roboData, &roboStatus, &roboTimers); 
      
      //Serial.println(roboData.yawrotation);
      //Serial.println(roboData.yawCompensation);

      digitalWrite(12,LOW);
    }
    // Hold
    while(1);    
  }
  return;
}


/**
 * Brief:Update Heading
 * Notes:
 */
 
void update_heading(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  if((millis() - (Timing->cycleTimer)) < CYCLETIME)return;
  Timing->cycleTimer = millis();
  
  float tempRotationValues[3] = {0};
  sixDOF.getEuler(tempRotationValues);
  
  Robot->yawrotation = (int32_t)(tempRotationValues[0] * 100);// - Robot->yawCompensation; // This method of drift compenstaion is cooked.!!!!!!!!!!!!!!!!!!!!!!!!
  Robot->yawrotation += 18000;
  if(((Robot->yawrotation - Robot->previousyawrotation) > 50) | ((Robot->yawrotation - Robot->previousyawrotation) < -50) | (Status->robotStatus & STATUS_IS_MOVING))
  {
    digitalWrite(13,LOW);
    Status->robotStatus ^= (STATUS_ALIGNING);
    Robot->previousyawrotation = Robot->yawrotation;
    return;  
  }
  
  if(Robot->yawrotation > Robot->previousyawrotation)
  {
    digitalWrite(13,HIGH);
    Status->robotStatus |= (STATUS_ALIGNING);
    Robot->yawCompensation++;
    return;
  }
  
  if(Robot->yawrotation < Robot->previousyawrotation)
  {
    digitalWrite(13,HIGH);
    Status->robotStatus |= (STATUS_ALIGNING);
    Robot->yawCompensation--;   
    return;
  }
  digitalWrite(13,LOW);
  Status->robotStatus ^= (STATUS_ALIGNING);
  return;  
}

/**
 * Brief:Drive Forward
 * Notes:
 */
 
void forward(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    Serial.println("FORWARD");
    Serial.print(movementTime);
    Serial.println(" mSec");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_FORWARD_VALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_FORWARD_VALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    Serial.println("STOP FORWARD");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Stop
 * Notes:
 */
 
void allStop(RobotStatus_Typedef *Status)
{
  Serial.print("ALL STOP");
  servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
  servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
  servoLeft.detach();
  servoRight.detach();
  Status->robotStatus ^= STATUS_IS_MOVING;
  return;
}

/**
 * Brief:Turn Right
 * Notes: What if target is less than -180 or greater than 180?
 */

void turnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {

    Serial.print("TURN_RIGHT: ");
    Serial.print(turnAngle);
    Serial.println(" x .01 Deg");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
    
    Robot->targetrotation = Robot->yawrotation + (turnAngle);
    
    Serial.println(Robot->yawrotation);
    Serial.println(Robot->targetrotation);
    
    if(Robot->targetrotation > 36000)
    {
      Status->robotFlagSet |= FLAG_SET_TURN_ROLL_OVER;
      Robot->targetrotation = ((Robot->targetrotation) - 36000);
    }    
    Serial.println(Robot->targetrotation);
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_RHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_RHT_TURNVALUE);
    return;
  }
  if ( Status->robotFlagSet & FLAG_SET_TURN_ROLL_OVER )
  {
    if ( Robot->yawrotation > Robot->targetrotation )return;
    Status->robotFlagSet ^= FLAG_SET_TURN_ROLL_OVER; 
  }
  if((Robot->yawrotation > Robot->targetrotation))
  {
    Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief: Turn Right
 * Notes: Durational
 */
 
void turnRight(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    Serial.println("RIGHT TURN");
    Serial.print(movementTime);
    Serial.println(" mSec");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_RHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_RHT_TURNVALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Turn Left
 * Notes: What if target is less than -180 or greater than 180?
 */

void turnLeft(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {

    Serial.print("TURN_LEFT: ");
    Serial.print(turnAngle);
    Serial.println(" x .01 Deg");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
    
    Robot->targetrotation = Robot->yawrotation - (turnAngle);
    
    Serial.println(Robot->yawrotation);
    Serial.println(Robot->targetrotation);
    
    if(Robot->targetrotation < 0)
    {
      Status->robotFlagSet |= FLAG_SET_TURN_ROLL_OVER;
      Robot->targetrotation = ((Robot->targetrotation) + 36000);
    }    
    Serial.println(Robot->targetrotation);
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_LHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_LHT_TURNVALUE);
    return;
  }
  if ( Status->robotFlagSet & FLAG_SET_TURN_ROLL_OVER )
  {
    if ( Robot->yawrotation < Robot->targetrotation )return;
    Status->robotFlagSet ^= FLAG_SET_TURN_ROLL_OVER; 
  }
  if((Robot->yawrotation < Robot->targetrotation))
  {
    Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief: Turn Left
 * Notes: Durational
 */

void turnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    Serial.println("LEFT TURN");
    Serial.print(movementTime);
    Serial.println(" mSec");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_LHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_LHT_TURNVALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Forward
 * Notes:
 */
 
void reverse(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    Serial.println("REVERSE");
    Serial.print(movementTime);
    Serial.println(" mSec");
    
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(12);
    servoRight.attach(13);
    servoRight.writeMicroseconds(MOTOR_RIGHT_REV_VALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_REV_VALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    Serial.println("STOP REVERSE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus ^= STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Forward
 * Notes:
 */
 
void updateSensors()
{
  return;
}
