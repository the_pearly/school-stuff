/**
 * Project Description :
 * Authors :
 * 
 */

/**
 * Brief:Sensor Includes
 * Notes:
 */

#include "imu_roboto.h"

/**
 * Brief:Other Includes
 * Notes:
 */

#include <Wire.h>
#include <Servo.h>

/**
 * Brief:Peripheral Management Objects
 * Notes:
 */

FreeSixIMU sixDOF = FreeSixIMU();
DFRobot_QMC5883 compass;

Servo servoRight;
Servo servoLeft;
Servo servoSonar;

/**
 * Brief:Setup
 * Notes:
 */

/**
 * Brief:Prototype Functions
 * Notes:
 */

void switchSanityTest(RobotData_Typedef *roboData);
void programNavigate(RobotData_Typedef *roboData, RobotStatus_Typedef *roboStatus, RobotTimers_Typedef *roboTimers);
void programScript(RobotData_Typedef *roboData, RobotStatus_Typedef *roboStatus, RobotTimers_Typedef *roboTimers); 
void update_heading(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void forward(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void forwardUntilDetect(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void forward(RobotStatus_Typedef *Status);
void turnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000);
void turnRight(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void turnLeft(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000);
void turnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void reverse(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void slowTurnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000);
void slowTurnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000);
void updateSensors(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void rest(RobotStatus_Typedef *Status , RobotTimers_Typedef *Timing, uint16_t restingTime = 1000);
void sonarSweep(RobotData_Typedef *Robot, RobotStatus_Typedef *Status);
void centreSonar();
void sonarDetectFront(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void processSweepResults(RobotData_Typedef *Robot, RobotStatus_Typedef *Status);
void Navigate(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void NavigateAvoid(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);
void NavigateAhead(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing);

void setup() 
{
  return;
}


/**
 * Brief: PROGRAM ENTRY
 * Notes: Enter up to 16 program actions into this array.
 *        See imu_robot.h for action codes
 */


uint8_t Program[TOTAL_PROGRAM_SLOTS][TOTAL_PROGRAM_LINES][3] = 
{{
  // Action Code , Duration/Angle/Sensor
//// PROGRAM A //// TRIANGLE
/* LINE 0 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 1 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 2 */  { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_120 , _NULL},
/* LINE 3 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 4 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 5 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 6 */  { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_120 , _NULL},
/* LINE 7 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 8 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 9 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 10 */ { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_120 , _NULL},
/* LINE 11 */ { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 12 */ { ACTION_CODE_PROGRAM_END , _NULL , _NULL},
/* LINE 13 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 14 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 15 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL}
},
{
//// PROGRAM B //// SQUARE
  // Action Code , Duration/Angle/Sensor
/* LINE 0 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 1 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 2 */  { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_90 , _NULL},
/* LINE 3 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 4 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 5 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 6 */  { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_90 , _NULL},
/* LINE 7 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 8 */  { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 9 */  { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 10 */ { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_90 , _NULL},
/* LINE 11 */ { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 12 */ { ACTION_CODE_FORWARD , DURATION_1M , _NULL},
/* LINE 13 */ { ACTION_CODE_REST , WAIT_DURATION , _NULL},
/* LINE 14 */ { ACTION_CODE_TURNLEFT_ANGLE , ANGLE_90 , _NULL},
/* LINE 15 */ { ACTION_CODE_PROGRAM_END , _NULL , _NULL}
},
{
//// PROGRAM C // CIRCLE
  // Action Code , Duration/Angle/Sensor
/* LINE 0 */  { ACTION_CODE_SLOWTURN_RIGHT , ANGLE_SLOW , _NULL},
/* LINE 1 */  { ACTION_CODE_REST , 0 , _NULL},
/* LINE 2 */  { ACTION_CODE_SLOWTURN_RIGHT , ANGLE_SLOW , _NULL},
/* LINE 3 */  { ACTION_CODE_REST , 0 , _NULL},
/* LINE 4 */  { ACTION_CODE_SLOWTURN_RIGHT , ANGLE_SLOW , _NULL},
/* LINE 5 */  { ACTION_CODE_REST , 0 , _NULL},
/* LINE 6 */  { ACTION_CODE_SLOWTURN_RIGHT , ANGLE_SLOW , _NULL},
/* LINE 7 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 8 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 9 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 10 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 11 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 12 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 13 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 14 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 15 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL}
},

//// PROGRAM C // CIRCLE
  // Action Code , Duration/Angle/Sensor
///* LINE 0 */  { ACTION_CODE_SLOWTURN_LEFT , CIRCLE_DURATION , _NULL},
///* LINE 1 */  { ACTION_CODE_SLOWTURN_LEFT , CIRCLE_DURATION , _NULL},
///* LINE 2 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 3 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 4 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 5 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 6 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 7 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 8 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 9 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 10 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 11 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 12 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 13 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 14 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
///* LINE 15 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL}
//},



{
//// PROGRAM THREE
  // Action Code , Duration/Angle/Sensor
/* LINE 0 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 1 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 2 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 3 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 4 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 5 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 6 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 7 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 8 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 9 */  { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 10 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 11 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 12 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 13 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 14 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL},
/* LINE 15 */ { ACTION_CODE_ALLSTOP , _NULL , _NULL}
}
};

/**
 * Brief:Main Loop
 * Notes:
 */

void loop() 
{
  
  RobotData_Typedef roboData;
  RobotStatus_Typedef roboStatus;
  RobotTimers_Typedef roboTimers;

  //roboData.declinationAngle = (DEC_ANGLE_DEG + (DEC_ANGLE_MIN / 60.0)) / (180 / PI);
  roboData.programSwitchValue = ( (digitalRead(PROGRAM_SELECT_SWITCH_A)<<PROGRAM_BIT_A) | (digitalRead(PROGRAM_SELECT_SWITCH_B)<<PROGRAM_BIT_B) ); 
  //roboData.programSwitchValue = PROGRAM_D;
  Serial.begin(115200);
  Wire.begin();
  compass.begin();
  
  delay(5);
  sixDOF.init(); //begin the IMU
  delay(5);

  pinMode(STATUS_LED,OUTPUT);
  pinMode(DEBUG_CYCLE_TRIGGER,OUTPUT);
  pinMode(HC_SR04_TRIGGER, OUTPUT);
  pinMode(HC_SRO4_ECHO, INPUT);
  pinMode(RIGHT_INDICATOR_LIGHT, OUTPUT);
  pinMode(LEFT_INDICATOR_LIGHT, OUTPUT);

  digitalWrite(RIGHT_INDICATOR_LIGHT, LOW);
  digitalWrite(LEFT_INDICATOR_LIGHT, LOW);

  switchSanityTest(&roboData);
  
  if(roboData.programSwitchValue == PROGRAM_D)Navigate(&roboData, &roboStatus, &roboTimers);
  programScript(&roboData, &roboStatus, &roboTimers);
  
  while(1);
  
}

void switchSanityTest(RobotData_Typedef *roboData)
{
  digitalWrite(LEFT_INDICATOR_LIGHT, (roboData->programSwitchValue & 1<<PROGRAM_BIT_A));
  digitalWrite(RIGHT_INDICATOR_LIGHT, (roboData->programSwitchValue & 1<<PROGRAM_BIT_B));
  delay(2000);
  return;
}

void programNavigate(RobotData_Typedef *roboData, RobotStatus_Typedef *roboStatus, RobotTimers_Typedef *roboTimers)
{
  //centreSonar();

    {
      switch ( roboData->navigationCommand[0] )
      {
        case _NULL:
          break;
        case ACTION_CODE_ALLSTOP:
          allStop(roboStatus);          
          break;
          
        case ACTION_CODE_FORWARD:
          forward( roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;

        case ACTION_CODE_FORWARD_DETECT:
          forwardUntilDetect( roboData, roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;
          
        case ACTION_CODE_TURNRIGHT_ANGLE:
          turnRight( roboData, roboStatus, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;

        case ACTION_CODE_TURNLEFT_ANGLE:
          turnLeft( roboData, roboStatus, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;

        case ACTION_CODE_TURNRIGHT_DUR:
          turnRight( roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;
          
        case ACTION_CODE_TURNLEFT_DUR:
          turnLeft( roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;
          
        case ACTION_CODE_REVERSE:
          reverse( roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;

        case ACTION_CODE_REST:
          rest( roboStatus, roboTimers, ((uint16_t)(roboData->navigationCommand[1]*100)) );          
          break;

        case ACTION_CODE_SENSE:
          updateSensors(roboData, roboStatus, roboTimers);
          break;

        case ACTION_CODE_PROGRAM_END:
          return;          
      }
      
      digitalWrite(DEBUG_CYCLE_TRIGGER,HIGH);
      
      update_heading(roboData, roboStatus, roboTimers);       
      /*
      if(VERBOSE)
      {
        Serial.print(roboData->navigationCommand[0],HEX);
        Serial.print(" For : ");
        Serial.println(roboData->navigationCommand[1]);
      }
      */
      digitalWrite(DEBUG_CYCLE_TRIGGER,LOW); 
    }
  return;
}

void programScript(RobotData_Typedef *roboData, RobotStatus_Typedef *roboStatus, RobotTimers_Typedef *roboTimers)
{
  centreSonar();

  Serial.print("Running >> Program : ");
  Serial.println((roboData->programSwitchValue + 10), HEX ); 

    for (roboData->programLineCount = 0; roboData->programLineCount < TOTAL_PROGRAM_LINES;)
    {
      switch ( Program[roboData->programSwitchValue][roboData->programLineCount][0] )
      {
        case _NULL:
          break;
          
        case ACTION_CODE_ALLSTOP:
          allStop(roboStatus);
          roboData->programLineCount++;
          break;
          
        case ACTION_CODE_FORWARD:
          forward(roboStatus, roboTimers, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;
          
        case ACTION_CODE_TURNRIGHT_ANGLE:
          turnRight(roboData, roboStatus, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)) );
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;

        case ACTION_CODE_TURNLEFT_ANGLE:
          turnLeft(roboData, roboStatus, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)) );
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;

        case ACTION_CODE_TURNRIGHT_DUR:
          turnRight(roboStatus, roboTimers, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;
          
        case ACTION_CODE_TURNLEFT_DUR:
          turnLeft(roboStatus, roboTimers, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;
          
        case ACTION_CODE_REVERSE:
          reverse(roboStatus, roboTimers, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;

        case ACTION_CODE_SLOWTURN_LEFT:
          slowTurnLeft(roboStatus, roboTimers, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;

        case ACTION_CODE_SLOWTURN_RIGHT:
          slowTurnRight(roboData, roboStatus, ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]*100)));
          if ( !((roboStatus->robotStatus) & (STATUS_IS_MOVING)) )roboData->programLineCount++;
          break;

        case ACTION_CODE_REST:
          rest(roboStatus, roboTimers), ((uint16_t)(Program[roboData->programSwitchValue][roboData->programLineCount][1]));
          if ( !((roboStatus->robotStatus) & (STATUS_ROBOT_REST)) )roboData->programLineCount++;
          break;
                  
        case ACTION_CODE_SENSE:
          updateSensors(roboData, roboStatus, roboTimers);
          roboData->programLineCount++;
          break;

        case ACTION_CODE_GOTO_LINE:
          roboData->programLineCount = Program[roboData->programSwitchValue][roboData->programLineCount][1];
          break;

        case ACTION_CODE_PROGRAM_END:
          allStop(roboStatus);
          if(VERBOSE)Serial.println("PROGRAM TERMINATE");
          while(1)
          {
            digitalWrite(STATUS_LED, HIGH);
            delay(250);
            digitalWrite(STATUS_LED, LOW);
            delay(250);
          }
          break;
          
      }
      
      digitalWrite(DEBUG_CYCLE_TRIGGER,HIGH);
      update_heading(roboData, roboStatus, roboTimers); 
      
      //Serial.println(roboData.yawrotation);
      //Serial.println(roboData.yawCompensation);

      digitalWrite(DEBUG_CYCLE_TRIGGER,LOW);

    // Hold
    
    }
  return;
}


/**
 * Brief:Update Heading
 * Notes:
 */
 
void update_heading(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  if((millis() - (Timing->cycleTimer)) < CYCLETIME)return;
  Timing->cycleTimer = millis();
  
  float tempRotationValues[3] = {0};
  sixDOF.getEuler(tempRotationValues);
  
  Robot->yawrotation = (int32_t)(tempRotationValues[0] * 100);// - Robot->yawCompensation; // This method of drift compenstaion is cooked.!!!!!!!!!!!!!!!!!!!!!!!!
  Robot->yawrotation += 18000;
  if(((Robot->yawrotation - Robot->previousyawrotation) > 50) | ((Robot->yawrotation - Robot->previousyawrotation) < -50) | (Status->robotStatus & STATUS_IS_MOVING))
  {
    digitalWrite(STATUS_LED,LOW);
    Status->robotStatus &= ~(STATUS_ALIGNING);
    Robot->previousyawrotation = Robot->yawrotation;
    return;  
  }
  
  if(Robot->yawrotation > Robot->previousyawrotation)
  {
    digitalWrite(STATUS_LED,HIGH);
    Status->robotStatus |= (STATUS_ALIGNING);
    Robot->yawCompensation++;
    return;
  }
  
  if(Robot->yawrotation < Robot->previousyawrotation)
  {
    digitalWrite(STATUS_LED,HIGH);
    Status->robotStatus |= (STATUS_ALIGNING);
    Robot->yawCompensation--;   
    return;
  }
  digitalWrite(STATUS_LED,LOW);
  Status->robotStatus &= ~(STATUS_ALIGNING);
  return;  
}

/**
 * Brief:Drive Forward
 * Notes:
 */
 
void forward(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    if(VERBOSE)
    {
      Serial.println("FORWARD");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);   
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_FORWARD_VALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_FORWARD_VALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  if(VERBOSE)Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("STOP FORWARD");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Forward
 * Notes:
 */
 
void forwardUntilDetect(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    //centreSonar();
    if(VERBOSE)
    {
      Serial.println("FORWARD");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);   
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoSonar.attach(SONAR_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_FORWARD_VALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_FORWARD_VALUE); 
    servoSonar.writeMicroseconds(SCAN_ANGLE_90_PWM);    
    Timing->movementTimer = millis();
    return;
  }  
  if(VERBOSE)Serial.println(millis() - Timing->movementTimer);
  sonarDetectFront(Robot, Status, Timing);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("STOP FORWARD");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    servoSonar.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }  
  else if((Status->robotFlagSet) & FLAG_SET_OBSTACLE_DETECTED_FRONT)
  {
    if(VERBOSE)Serial.println("STOP FORWARD");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    servoSonar.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Stop
 * Notes:
 */
 
void allStop(RobotStatus_Typedef *Status)
{
  if(VERBOSE)Serial.println("ALL STOP");
  servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
  servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
  servoLeft.detach();
  servoRight.detach();
  Status->robotStatus &= ~STATUS_IS_MOVING;
  return;
}

/**
 * Brief:Turn Right
 * Notes: What if target is less than -180 or greater than 180?
 */

void turnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    
    Status->robotStatus |= (STATUS_IS_MOVING);
    
    Robot->targetrotation = Robot->yawrotation + (turnAngle);
    if(VERBOSE)
    {
      Serial.print("TURN_RIGHT: ");
      Serial.print(turnAngle);
      Serial.println(" x .01 Deg");    
      Serial.println(Robot->yawrotation);
      Serial.println(Robot->targetrotation);
    }
    if(Robot->targetrotation > 36000)
    {
      Status->robotFlagSet |= FLAG_SET_TURN_ROLL_OVER;
      Robot->targetrotation = ((Robot->targetrotation) - 36000);
    }    
    if(VERBOSE)Serial.println(Robot->targetrotation);
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_RHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_RHT_TURNVALUE);
    return;
  }
  if(VERBOSE)Serial.println(Robot->yawrotation);
  if ( Status->robotFlagSet & FLAG_SET_TURN_ROLL_OVER )
  {
    if ( Robot->yawrotation > Robot->targetrotation )return;
    Status->robotFlagSet &= ~FLAG_SET_TURN_ROLL_OVER; 
  }
  if((Robot->yawrotation > Robot->targetrotation))
  {
    if(VERBOSE)Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief: Turn Right
 * Notes: Durational
 */
 
void turnRight(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    if(VERBOSE)
    {
      Serial.println("RIGHT TURN");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_RHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_RHT_TURNVALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  //Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Turn Left
 * Notes: What if target is less than -180 or greater than 180?
 */

void turnLeft(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {

    Status->robotStatus |= (STATUS_IS_MOVING);
    
    Robot->targetrotation = Robot->yawrotation - (turnAngle);
    if(VERBOSE)
    {
      Serial.print("TURN_LEFT: ");
      Serial.print(turnAngle);
      Serial.println(" x .01 Deg");  
      Serial.println(Robot->yawrotation);
      Serial.println(Robot->targetrotation);
    }
    if(Robot->targetrotation < 0)
    {
      Status->robotFlagSet |= FLAG_SET_TURN_ROLL_OVER;
      Robot->targetrotation = ((Robot->targetrotation) + 36000);
    }    
    if(VERBOSE)Serial.println(Robot->targetrotation);
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_LHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_LHT_TURNVALUE);
    return;
  }
  if ( Status->robotFlagSet & FLAG_SET_TURN_ROLL_OVER )
  {
    if ( Robot->yawrotation < Robot->targetrotation )return;
    Status->robotFlagSet &= ~FLAG_SET_TURN_ROLL_OVER; 
  }
  if((Robot->yawrotation < Robot->targetrotation))
  {
    if(VERBOSE)Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief: Turn Left
 * Notes: Durational
 */

void turnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    if(VERBOSE)
    {
      Serial.println("LEFT TURN");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_LHT_TURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_LHT_TURNVALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  //Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

/**
 * Brief:Drive Reverse
 * Notes:
 */
 
void reverse(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    if(VERBOSE)
    {
      Serial.println("REVERSE");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_REV_VALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_REV_VALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  //Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("STOP REVERSE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}


void slowTurnRight(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, uint16_t turnAngle = 9000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    
    Status->robotStatus |= (STATUS_IS_MOVING);
    
    Robot->targetrotation = Robot->yawrotation + (turnAngle);
    if(VERBOSE)
    {
      Serial.print("TURN_RIGHT: ");
      Serial.print(turnAngle);
      Serial.println(" x .01 Deg");    
      Serial.println(Robot->yawrotation);
      Serial.println(Robot->targetrotation);
    }
    if(Robot->targetrotation > 36000)
    {
      Status->robotFlagSet |= FLAG_SET_TURN_ROLL_OVER;
      Robot->targetrotation = ((Robot->targetrotation) - 36000);
    }    
    if(VERBOSE)Serial.println(Robot->targetrotation);
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_RHT_SLOWTURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_RHT_SLOWTURNVALUE);
    return;
  }
  if(VERBOSE)Serial.println(Robot->yawrotation);
  if ( Status->robotFlagSet & FLAG_SET_TURN_ROLL_OVER )
  {
    if ( Robot->yawrotation > Robot->targetrotation )return;
    Status->robotFlagSet &= ~FLAG_SET_TURN_ROLL_OVER; 
  }
  if((Robot->yawrotation > Robot->targetrotation))
  {
    if(VERBOSE)Serial.println("TURN COMPLETE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}

void slowTurnLeft(RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing, uint16_t movementTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_IS_MOVING)) )
  {
    if(VERBOSE)
    {
      Serial.println("VEER LEFT");
      Serial.print(movementTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_IS_MOVING);
        
    servoLeft.attach(LEFT_SERVOMOTOR);
    servoRight.attach(RIGHT_SERVOMOTOR);
    servoRight.writeMicroseconds(MOTOR_RIGHT_LHT_SLOWTURNVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_LHT_SLOWTURNVALUE);    
    Timing->movementTimer = millis();
    return;
  }  
  //Serial.println(millis() - Timing->movementTimer);
  if((millis() - (Timing->movementTimer)) > movementTime)
  {
    if(VERBOSE)Serial.println("STOP REVERSE");
    servoRight.writeMicroseconds(MOTOR_RIGHT_STOPVALUE);
    servoLeft.writeMicroseconds(MOTOR_LEFT_STOPVALUE);
    servoLeft.detach();
    servoRight.detach();
    Status->robotStatus &= ~STATUS_IS_MOVING;   
  }
  return;
}
/**
 * Brief:Update Sensors
 * Notes:
 */

void updateSensors(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  if(VERBOSE)
  {
    Serial.println("SENSING");
  }
  sonarSweep(Robot, Status);
  processSweepResults(Robot, Status);
  return;
}

/**
 * Brief: Rest or wait specified time
 * Notes:
 */
 
void rest(RobotStatus_Typedef *Status , RobotTimers_Typedef *Timing, uint16_t restingTime = 1000)
{
  if( !((Status->robotStatus) & (STATUS_ROBOT_REST)) )
  {
    if(VERBOSE)
    {
      Serial.println("RESTING");
      Serial.print(restingTime);
      Serial.println(" mSec");
    }
    Status->robotStatus |= (STATUS_ROBOT_REST);   
    Timing->movementTimer = millis();
    return;
  }
  if((millis() - (Timing->movementTimer)) > restingTime)
  {
    if(VERBOSE)Serial.println("STOP REST");
    Status->robotStatus &= ~STATUS_ROBOT_REST;   
  }  
  return;
}

/**
 * Brief: Ultrasonic Sweep
 * Notes: Perform ultrasonice Rangefiner sweep and store results in table.
 */
 
void sonarSweep(RobotData_Typedef *Robot, RobotStatus_Typedef *Status)
{
  servoSonar.attach(SONAR_SERVOMOTOR);
  uint32_t sonar_Echo_Duration = 0;  
  uint8_t sonarPositionCounter = SCAN_ARRAY_POSITION_15;
  while(sonarPositionCounter < SCAN_ARRAY_POSITION_165)
  {   
      sonarPositionCounter++;   
      servoSonar.writeMicroseconds(Robot->sonar_Position_Angle[sonarPositionCounter]);              
      delay(SONAR_SWEEP_DELAY);
      
      digitalWrite(HC_SR04_TRIGGER, LOW);  
      delayMicroseconds(2); 
      digitalWrite(HC_SR04_TRIGGER, HIGH);
      delayMicroseconds(10); 
      digitalWrite(HC_SR04_TRIGGER, LOW);      
      sonar_Echo_Duration = pulseIn(HC_SRO4_ECHO, HIGH);
      
      Robot->sonar_Distance_Measurement[ROW_ZERO][sonarPositionCounter] = ((sonar_Echo_Duration) / (60));
      if(VERBOSE)Serial.println(Robot->sonar_Distance_Measurement[ROW_ZERO][sonarPositionCounter]); 
      delay(150);
  }
    
  while(sonarPositionCounter > SCAN_ARRAY_POSITION_15)
  {   
      sonarPositionCounter--;      
      servoSonar.writeMicroseconds(Robot->sonar_Position_Angle[sonarPositionCounter]);            
      delay(SONAR_SWEEP_DELAY);
      digitalWrite(HC_SR04_TRIGGER, LOW);  // Added this line
      delayMicroseconds(2); // Added this line
      digitalWrite(HC_SR04_TRIGGER, HIGH);
      delayMicroseconds(10); // Added this line
      digitalWrite(HC_SR04_TRIGGER, LOW);
      sonar_Echo_Duration = pulseIn(HC_SRO4_ECHO, HIGH);
      
      Robot->sonar_Distance_Measurement[ROW_ONE][sonarPositionCounter] = ((sonar_Echo_Duration) / (60));
      if(VERBOSE)Serial.println(Robot->sonar_Distance_Measurement[ROW_ONE][sonarPositionCounter]); 
      delay(150);
  }
  centreSonar();
  return;
}

/**
 * Brief: Center Rangefinder
 * Notes: 
 */
 
void centreSonar()
{
  servoSonar.attach(SONAR_SERVOMOTOR);
  servoSonar.writeMicroseconds(SCAN_ANGLE_90_PWM);
  delay(150);
  servoSonar.detach();
  return;
}

/**
   Brief: Detect Front.
   Notes: Set collision flag if distance to object in front below allowable
*/

void sonarDetectFront(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  if((millis() - Timing->scanTimer) > SONAR_POSITION_HOLD_DELAY)
  {
    Timing->scanTimer = millis();
    Robot->frontScanPositionCounter += Robot->frontScanDirection;
    if(Robot->frontScanPositionCounter > SCAN_ARRAY_POSITION_120)
    {
      Robot->frontScanPositionCounter = SCAN_ARRAY_POSITION_105;
      Robot->frontScanDirection = _DOWN;
    }
    if(Robot->frontScanPositionCounter < SCAN_ARRAY_POSITION_60)
    {
      Robot->frontScanPositionCounter = SCAN_ARRAY_POSITION_75;
      Robot->frontScanDirection = _UP;
    }
    if(VERBOSE)Serial.println(Robot->sonar_Position_Angle[Robot->frontScanPositionCounter]);
    servoSonar.attach(SONAR_SERVOMOTOR);
    servoSonar.writeMicroseconds(Robot->sonar_Position_Angle[Robot->frontScanPositionCounter]);
    delay(SONAR_SWEEP_DELAY);
    
    servoSonar.detach();
  }
  delay(SONAR_SWEEP_DELAY);
  uint32_t sonar_Echo_Duration = 0;
  
  digitalWrite(HC_SR04_TRIGGER, LOW);  // Added this line
  delayMicroseconds(2); // Added this line
  digitalWrite(HC_SR04_TRIGGER, HIGH);
  delayMicroseconds(10); // Added this line
  digitalWrite(HC_SR04_TRIGGER, LOW);  
  sonar_Echo_Duration = pulseIn(HC_SRO4_ECHO, HIGH);
  Robot->sonar_Distance_Measurement[ROW_ZERO][SCAN_ARRAY_POSITION_90] = ((sonar_Echo_Duration) / (60));
  
  if ((Robot->sonar_Distance_Measurement[ROW_ZERO][SCAN_ARRAY_POSITION_90]) < (MINIMUM_ALLOWABLE_DISTANCE_FRONT ))
  {  
    Status->robotFlagSet |= FLAG_SET_OBSTACLE_DETECTED_FRONT;
    if(VERBOSE)Serial.println("COLLISION DETECT FRONT"); 
  }
  return;
}

/**
  Function: Checks sonar array for Objects
  Inputs: Data and status structs
  Output: Sets object detection flags 
*/

void processSweepResults(RobotData_Typedef *Robot, RobotStatus_Typedef *Status)
{
  // Clear Flags
  Status->robotFlagSet &= ~(FLAG_SET_OBSTACLE_DETECTED_RIGHT 
                          | FLAG_SET_OBSTACLE_DETECTED_LEFT 
                          | FLAG_SET_OBSTACLE_DETECTED_FRONT 
                          | FLAG_SET_BEST_DIRECTION_LEFT 
                          | FLAG_SET_BEST_DIRECTION_RIGHT);
  // Average Results
  
  for(uint8_t positionCounter = SCAN_ARRAY_POSITION_15; positionCounter < SCAN_ARRAY_POSITION_165; positionCounter++)
  {
    Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] = ((Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] + Robot->sonar_Distance_Measurement[ROW_ONE][positionCounter]) / NUMBER_OF_SCAN_READINGS );      
  }
  // Find best left hand path
  uint16_t tempDistanceLeft = 0;
  for(uint8_t positionCounter = SCAN_ARRAY_POSITION_15; positionCounter < SCAN_ARRAY_POSITION_60; positionCounter++)
  {
    if (Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] < MINIMUM_ALLOWABLE_DISTANCE_LEFT)
    {
      Status->robotFlagSet |= FLAG_SET_OBSTACLE_DETECTED_LEFT;
      if(VERBOSE)Serial.println("COLLISION DETECT LEFT"); 
    }
    if(Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] > tempDistanceLeft)
    {
      tempDistanceLeft = Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter];      
      Robot->bestPathLeft = Robot->sonar_Position_Value[positionCounter];      
    }
  }  
  // find best Right Hand Path
  uint16_t tempDistanceRight = 0;
  for(uint8_t positionCounter = SCAN_ARRAY_POSITION_120; positionCounter < SCAN_ARRAY_POSITION_165; positionCounter++)
  {
    if (Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] < MINIMUM_ALLOWABLE_DISTANCE_RIGHT)
    {
      Status->robotFlagSet |= FLAG_SET_OBSTACLE_DETECTED_RIGHT;
      if(VERBOSE)Serial.println("COLLISION DETECT RIGHT"); 
      break;
    }
    if(Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter] > tempDistanceRight)
    {
      tempDistanceRight = Robot->sonar_Distance_Measurement[ROW_ZERO][positionCounter];      
      Robot->bestPathRight = Robot->sonar_Position_Value[positionCounter];      
    }
  }
  
  if(tempDistanceRight > tempDistanceLeft)Status->robotFlagSet |= FLAG_SET_BEST_DIRECTION_RIGHT;
  else if(tempDistanceLeft > tempDistanceRight)Status->robotFlagSet |= FLAG_SET_BEST_DIRECTION_LEFT;
  
    

  if(VERBOSE)
  {
    Serial.print("Best Left Path ");
    Serial.println(Robot->bestPathLeft);
    Serial.print("Best Right Path ");
    Serial.println(Robot->bestPathRight);
  }
  
  if (Robot->sonar_Distance_Measurement[ROW_ZERO][SCAN_ARRAY_POSITION_90] < MINIMUM_ALLOWABLE_DISTANCE_FRONT)
  {  
    Status->robotFlagSet |= FLAG_SET_OBSTACLE_DETECTED_FRONT;
    if(VERBOSE)Serial.println("COLLISION DETECT FRONT"); 
  }

  digitalWrite(RIGHT_INDICATOR_LIGHT, (Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_RIGHT));
  digitalWrite(LEFT_INDICATOR_LIGHT, (Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_LEFT));
  return;
}

void NavigateAhead(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  Robot->navigationCommand[0] = ACTION_CODE_FORWARD_DETECT;
  Robot->navigationCommand[1] = 50;
  for(;;)
  {
    programNavigate(Robot, Status, Timing);
    if(!(Status->robotStatus & STATUS_IS_MOVING))break;
  }
  if(Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_FRONT)Status->robotFlagSet &= ~FLAG_SET_WAY_IS_CLEAR_AHEAD;
  return;
}

void NavigateAvoid(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
    if(Status->robotFlagSet & FLAG_SET_WAY_IS_CLEAR_AHEAD)return;

    Status->robotFlagSet &= ~FLAG_SET_WAY_IS_CLEAR_AHEAD;
    Robot->navigationCommand[0] = ACTION_CODE_REVERSE;
    Robot->navigationCommand[1] = 8;   
    
    while(1)
    { 
      for(;;)
      {
        programNavigate(Robot, Status, Timing);
        if(!(Status->robotStatus & STATUS_IS_MOVING))break;
      }

      if(Status->robotFlagSet & FLAG_SET_WAY_IS_CLEAR_AHEAD)return;

      Robot->navigationCommand[0] = ACTION_CODE_SENSE;
      Robot->navigationCommand[1] = _NULL; 
      programNavigate(Robot, Status, Timing);
      

      Robot->navigationCommand[0] = ACTION_CODE_TURNRIGHT_ANGLE;
      Robot->navigationCommand[1] = Robot->bestPathRight;
      Status->robotFlagSet |= FLAG_SET_WAY_IS_CLEAR_AHEAD;

      if( (!(Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_RIGHT)) && (Status->robotFlagSet & FLAG_SET_BEST_DIRECTION_RIGHT))
      {
          Robot->navigationCommand[0] = ACTION_CODE_TURNRIGHT_ANGLE;
          Robot->navigationCommand[1] = Robot->bestPathRight;
          Status->robotFlagSet |= FLAG_SET_WAY_IS_CLEAR_AHEAD;
      }
      else if( (!(Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_LEFT)) && (Status->robotFlagSet & FLAG_SET_BEST_DIRECTION_LEFT))
      {
          Robot->navigationCommand[0] = ACTION_CODE_TURNLEFT_ANGLE;
          Robot->navigationCommand[1] = Robot->bestPathLeft;
          Status->robotFlagSet |= FLAG_SET_WAY_IS_CLEAR_AHEAD;
      }         
      else if((Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_RIGHT) && (Status->robotFlagSet & FLAG_SET_OBSTACLE_DETECTED_LEFT))
      {
        Robot->navigationCommand[0] = ACTION_CODE_REVERSE;
        Robot->navigationCommand[1] = 5;
      }    
    }
    return;
}

void Navigate(RobotData_Typedef *Robot, RobotStatus_Typedef *Status, RobotTimers_Typedef *Timing)
{
  if(VERBOSE)Serial.print("Starting Navigation");
  Status->robotFlagSet |= FLAG_SET_WAY_IS_CLEAR_AHEAD;
  
  while(1)
  {
    // Drive Until Object
    NavigateAhead(Robot, Status, Timing);
    NavigateAvoid(Robot, Status, Timing); 
  }  
  return;
}
